<?php
/**
 * Created by PhpStorm.
 * User: Bincholero007
 * Date: 02/07/2018
 * Time: 22:05
 */

include'../database/Connexion_db.php';
/*
 * `ID_p`, `Nom_p`, `Prenoms_p`, `Sexe_p`, `DateNaiss_p`, `tel_p`, `Tel2_p`, `login`, `Profession_p`, `DomaineActivite_p`,
 *  `NomSociete_p`, `AdressSociete_p`, `TypePresta_p`, `EmailSociete_p`, `TelSociete_p`, `Siteweb_p`, `facebook`, `DescriptionSociete_p`, `Pays_p`, `Ville_p`,
 *  `Username_p`, `Password_p`, `Photo_p`, `DateAdhesion_p`, `statut`, `Localisation_p`, `position_p`, `calval`
 */
if(isset($_POST['search']) or $_POST['search']!="")
{
    $idPAss=$_GET['pw'];
    $sql_res=$db->prepare("select * from prestataire where ID_p=:ID_p AND actif_p=:actif_p ");
    $sql_res->execute(array('ID_p' =>$idPAss,'actif_p'=>'1'));
    $passwordGet=$sql_res->fetch();
    $pass=$passwordGet['Password_p'];
    $password=$_POST['search'];
    if (password_verify($password, $pass)) {
        echo '<em style="color: #2aa230">Mot de passe correct. vous pouvez modifier</em>';
        ?>
        <div class="form-group">
            <label class="bmd-label-floating">Nouveau mote de passe </label>
            <input type="password" class="form-control" name="password1" id="examplePassword" required>
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Confirmer le nouveau mot de passe</label>
            <input type="password" class="form-control" name="password2" equalTo="#examplePassword" id="examplePassword1" required>
        </div>
        <!---- ajax pour gere les adession au groupe  -----------------------------------------------------
        <script>
            $(document).ready(function(){
                /** ajouter affiche for user qui ajoute dans son groupe , il va chater avec eux after   **/
                $("#Add<?php echo $prestaGrp['ID_p']; ?>").click(function(e){
                    e.preventDefault();
                    $.post(
                        'template/ajax_updatePassword.php?passw=<?php echo $_GET['pw'];?>', // Un script PHP que l'on va créer juste après
                        {
                            idPw1: $("#examplePassword").val(),  // Nous récupérons la valeur de nos input que l'on fait passer à connexion.php
                            idPw2 : $("#examplePassword1").val()
                        },
                        function(data){
                            if(data == 'Success'){
                                // Le membre est connecté. Ajoutons lui un message dans la page HTML.
                                $("#resultat<?php echo $prestaGrp['ID_p']; ?>").html('<p align="center" style="color: #268b40">Ajouer avec succès </p>');

                            }else if(data == 'Erreur'){
                                // Le membre n'a pas été connecté. (data vaut ici "Erreur")
                                $("#resultat<?php echo $prestaGrp['ID_p']; ?>").html("<p style='color: #cd2d25'>Selectionnez un groupe</p>");
                            }else{
                                // Le membre n'a pas été connecté. (data vaut ici "failed")
                                $("#resultat<?php echo $prestaGrp['ID_p']; ?>").html("<p style='color: #cd2d25'>Action échoué</p>");
                            }
                        },
                        'text'
                    );
                });

            });
            function load_message_search(){
                $('#affichePartenairListe').load();
            }
        </script>
        <!---- fin ajax pour gere les adession au groupe  -------------------------------------------------->


    <?php
    }else{
        echo '<em style="color: #911a1c">votre mot de passe incorrect</em>';
    }
}
?>