<?php
/**
 * Created by PhpStorm.
 * User: CABINET NEVROGLIE
 * Date: 01/08/2018
 * Time: 11:18
 */

 $idPrestCoti=$_GET['etme'];
 $ID_userCotis=$_GET['etme'];
 $Select_Cotis_memberAll=Liste_Cotisation_memberAll($db,$idPrestCoti);
 $Select_Cotis_member_Nbr=Liste_Cotisation_member_Nbr($db,$idPrestCoti);
 $Index_Membre_cotisation=Indexe_Membre_Cotisation($db,$ID_userCotis);
?>

<div class="row">
    <div class="col-md-4">
        <div class="card00 " data-color="blue" id="wizardProfile">
            <form  method="post" action="">
                <div class="card card-login card-hidden">

                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-4" style="font-size: 15px">
                                <img class="img" src="img_upload/avatar/<?php echo $Index_Membre_cotisation['Photo_p']; ?>" style="border-radius: 0%; width: 150px;border: solid 1px"/>
                                <?php echo $Index_Membre_cotisation['DateAdhesion_p'];?>
                            </div>
                           <div class="col-md-1"></div>
                            <div class="col-md-7">
                                <p style="font-size: 14px; text-align: left">
                                    <?php echo
                                         ucwords($Index_Membre_cotisation['Nom_p']).'<br>
                                          '.ucfirst($Index_Membre_cotisation['Prenoms_p']).'<br>
                                          '.ucfirst($Index_Membre_cotisation['Profession_p']).'<br>
                                          '.ucfirst($Index_Membre_cotisation['TypePresta_p']).'<br>';
                                    ?>
                                    <?php if ($Index_Membre_cotisation['vip_user'] == '1') { ?>
                                        <img class="img" src="ressource/img/etoileP.png" style="width: 15px"/>
                                        <img class="img" src="ressource/img/etoileP.png" style="width: 15px"/>
                                        <img class="img" src="ressource/img/etoileP.png" style="width: 15px"/>
                                    <?php } else {
                                        if ($Index_Membre_cotisation['TypePresta_p'] == "Simple") {
                                            ?>
                                            <img class="img" src="ressource/img/etoileP.png" style="width: 15px"/>
                                            <img class="img" src="ressource/img/etoileV.png" style="width: 15px"/>
                                            <img class="img" src="ressource/img/etoileV.png" style="width: 15px"/>
                                        <?php
                                        } else {

                                            ?>
                                            <img class="img" src="ressource/img/etoileP.png" style="width: 15px"/>
                                            <img class="img" src="ressource/img/etoileP.png" style="width: 15px"/>
                                            <img class="img" src="ressource/img/etoileV.png" style="width: 15px"/>
                                        <?php
                                        }
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <input type="text" name="id_du_cotiseur" value="<?php echo $Index_Membre_cotisation['ID_p']; ?>" hidden="hidden">
                    <input type="text" name="nom_du_cotiseur" value="<?php echo $Index_Membre_cotisation['Nom_p'].' '.$Index_Membre_cotisation['Prenoms_p']; ?>" hidden="hidden">
                    <div class="card-footer justify-content-center">
                        <p>
                            <?php
                            if($Index_Membre_cotisation['vip_user']=='1'){
                                $btnvip='<button type="submit" class="btn btn-finish btn-fill btn-danger btn-wd" name="UpdateVIP">
                                                             <i class="material-icons">group_add</i>
                                                                  Rétirer du reseau d\'affaire
                                                            </button>';
                            }else{
                                $btnvip='<button type="submit" class="btn btn-finish btn-fill btn-default btn-wd" name="AddVIP">
                                                               <i class="material-icons">group_add</i>
                                                                    Ajouter au reseau d\'affaire
                                                             </button>';
                            }
                          //  echo $btnvip;
                            ?>

                        </p>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="col-md-8">
            <div class="card card-login card-hidden">
                <div class="card-body ">
                    <div class="material-datatables">

                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                            <tr style="font-weight: 700; background-color: #000000; color: #ffffff">
                                <th>Période D</th>
                                <th>Période F</th>
                                <th>Montant</th>
                                <th>Date</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php

                            foreach($Select_Cotis_memberAll as $profilViewCotis){
                                if($profilViewCotis['ModePayement']=='Cinetpay'){
                                    $satPay='<img src="ressource/img/staPay.png" style="width: 20px">';
                                    $staPayColor='style="background-color:rgba(69, 171, 251, 0.39) "';
                                }else{
                                    $satPay='<img src="ressource/img/staPayMy.png" style="width: 20px">';
                                    $staPayColor='style="background-color:rgba(254, 112, 29, 0.17) "';
                                }
                                ?>
                                <tr <?php echo $staPayColor; ?>  >
                                    <td><?php echo $profilViewCotis['dateCotisD']; ?></td>
                                    <td><?php echo $profilViewCotis['dateCotisF']; ?></td>
                                    <td><?php echo $profilViewCotis['SommCotis']; ?></td>
                                    <td><?php echo $profilViewCotis['dateEnrgCotis']; ?></td>

                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <hr>
                        <div class="text-right">
                            <em style="background-color: rgba(69, 171, 251, 0.39) ">payement effectué par le partenaire </em> |
                            <em style="background-color: rgba(254, 112, 29, 0.17)  ">payement effectué par CSN Admin</em>
                            <u>Total cotisation</u>: <b style="font-size: 18px; color: #c20d13" ><?php echo (int)$Index_Membre_cotisation['SomCotisMember']; ?></b> Fcfa
                        </div>

                    </div>

                </div>

    </div>

</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-icon card-header-rose">
                <div class="card-icon">
                    <i class="material-icons">timeline</i>
                </div>
                <h4 class="card-title"><small class="category" style="font-size: 19px">Bilan de la cotisation</small></h4>

            </div>
            <div class="card-body">

                <form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                Type membre: <b style="font-size: 17px"><?php echo $Index_Membre_cotisation['TypePresta_p']; ?></b><br>

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <canvas id="myChartBilanUser" width="700" height="150"></canvas>
                        </div>

                    </div>
                    <!--<button type="submit" class="btn btn-rose pull-right">Update Profile</button>-->
                    <div class="clearfix" style="margin: 0px"></div>
                </form>
            </div>
        </div>
    </div>
</div>
